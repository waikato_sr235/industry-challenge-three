package inventoryapp.storedetails;

import inventoryapp.exceptions.InventoryException;
import inventoryapp.product.ProductFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 *  Unit tests copied from/based on the unit tests in Challenge 1: Elapsed Time
 */
class TestLocalStore {
    private LocalStore sut;

    private PipedInputStream pIn;
    private PipedOutputStream pOut;
    private BufferedReader reader;

    private PrintStream oldOut;

    @BeforeEach
    void setUp() throws IOException, InventoryException {
        sut = new LocalStore("test", "test", 4);

        sut.addProduct(ProductFactory.create("Cleaning", "ajax", 1.0, 1));
        sut.addProduct(ProductFactory.create("Meat", "brisket", 1.0, 1, "2020-05-25", 1.0));
        sut.addProduct(ProductFactory.create("Bakery", "vegan brownie", 1.0, 1, "2020-05-25", 1.0, true));
        sut.addProduct(ProductFactory.create("Bakery", "croissant", 1.0, 1, "2020-05-25", 1.0, false));

        oldOut = System.out;

        // Hack so we can read things that you print using System.out.println
        pIn = new PipedInputStream();
        pOut = new PipedOutputStream(pIn);
        System.setOut(new PrintStream(pOut));
        reader = new BufferedReader(new InputStreamReader(pIn));
    }

    @AfterEach
    void tearDown()throws IOException {
        System.setOut(oldOut);
        reader.close();
        pOut.close();
    }

    @Test
    public void test_Max_Inventory_Exceeded() throws InventoryException {

        assertFalse(sut.addProduct(ProductFactory.create("Cleaning", "hand sanitiser", 1.0, 1)));
        assertFalse(sut.addProduct(ProductFactory.create("Produce", "feijoas", 1.0, 1, "2020-02-19", 1.0)));
        assertFalse(sut.addProduct(ProductFactory.create("Deli", "pastrami", 1.0, 1, "2020-02-19", 1.0, false)));
    }

    @Test
    public void test_Print_Info() throws IOException {
        sut.printInfo();

        assertEquals("ajax (Cleaning); price: $1.0; in stock: 1", reader.readLine());
        assertEquals("brisket (Meat); price: $1.0; in stock: 1; product weight: 1.0kg; expiry: 2020-05-25", reader.readLine());
        assertEquals("vegan brownie (Bakery); price: $1.0; in stock: 1; product weight: 1.0kg; expiry: 2020-05-25; is vegan", reader.readLine());
        assertEquals("croissant (Bakery); price: $1.0; in stock: 1; product weight: 1.0kg; expiry: 2020-05-25; is not vegan", reader.readLine());
        assertFalse(reader.ready());    //there should be nothing else to read

    }

    @Test
    public void test_Print_Category_Info() throws IOException {

        sut.printInfo("Cleaning");
        assertEquals("ajax (Cleaning); price: $1.0; in stock: 1", reader.readLine());
        assertFalse(reader.ready());    //there should be nothing else to read

        sut.printInfo("Meat");
        assertEquals("brisket (Meat); price: $1.0; in stock: 1; product weight: 1.0kg; expiry: 2020-05-25", reader.readLine());
        assertFalse(reader.ready());    //there should be nothing else to read

        sut.printInfo("Bakery");
        assertEquals("vegan brownie (Bakery); price: $1.0; in stock: 1; product weight: 1.0kg; expiry: 2020-05-25; is vegan", reader.readLine());
        assertEquals("croissant (Bakery); price: $1.0; in stock: 1; product weight: 1.0kg; expiry: 2020-05-25; is not vegan", reader.readLine());
        assertFalse(reader.ready());    //there should be nothing else to read
    }

}