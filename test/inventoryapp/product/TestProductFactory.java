package inventoryapp.product;

import inventoryapp.exceptions.InvalidCategoryException;
import inventoryapp.exceptions.InvalidDateException;
import inventoryapp.exceptions.InventoryException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestProductFactory {

    @Test
    public void test_Product_Factory_Creates_PerishableProduct() throws InventoryException {

        assertFalse(ProductFactory.create("Cleaning", "test", 1.0, 1) instanceof PerishableProduct);
        assertTrue(ProductFactory.create("Meat", "test", 1.0, 1, "2020-04-25", 1.0) instanceof PerishableProduct);
        assertTrue(ProductFactory.create("Deli", "test", 1.0, 1, "2020-04-25", 1.0, true) instanceof PerishableProduct);
    }

    @Test
    public void test_Product_Factory_Creates_VeganisableProduct() throws InventoryException {

        assertFalse(ProductFactory.create("Cleaning", "test", 1.0, 1) instanceof VeganisableProduct);
        assertFalse(ProductFactory.create("Meat", "test", 1.0, 1, "2020-04-25", 1.0) instanceof VeganisableProduct);
        assertTrue(ProductFactory.create("Deli", "test", 1.0, 1, "2020-04-25", 1.0, true) instanceof VeganisableProduct);
    }

    @Test
    public void test_Product_Factory_Invalid_Category_Input() {

        assertThrows(InvalidCategoryException.class, () -> ProductFactory.create("Trousers", "test", 1.0, 1));
        assertThrows(InvalidCategoryException.class, () -> ProductFactory.create("Cleaning", "test", 1.0, 1, "2020-04-25", 1.0));
        assertThrows(InvalidCategoryException.class, () -> ProductFactory.create("Meat", "test", 1.0, 1, "2020-04-25", 1.0, true));
    }

    @Test
    public void test_Product_Factory_Invalid_Expiry_Date_Input() {

        assertThrows(InvalidDateException.class, () -> ProductFactory.create("Meat", "test", 1.0, 1, "2020-02-31", 1.0));
        assertThrows(InvalidDateException.class, () -> ProductFactory.create("Deli", "test", 1.0, 1, "2020-09-31", 1.0, true));
    }

    @Test
    public void test_Product_Factory_Builds_Product() throws InventoryException {
        ProductFactory sut = new ProductFactory("Cleaning");

        sut.nextItem("ajax");
        sut.nextItem("1.0");
        sut.nextItem("1");

        Product product = sut.build();

        assertFalse(product instanceof PerishableProduct);
        assertEquals(new Product("Cleaning", "ajax", 1.0, 1), product);
    }

    @Test
    public void test_Product_Factory_Builds_PerishableProduct() throws InventoryException {
        ProductFactory sut = new ProductFactory("Meat");

        sut.nextItem("brisket");
        sut.nextItem("1.0");
        sut.nextItem("1");
        sut.nextItem("2020-04-25");
        sut.nextItem("1.0");

        Product product = sut.build();

        assertTrue(product instanceof PerishableProduct);
        assertFalse(product instanceof VeganisableProduct);
        assertEquals(new PerishableProduct("Meat", "brisket", 1.0, 1, "2020-04-25", 1.0), product);
    }

    @Test
    public void test_Product_Factory_Builds_VeganisableProduct() throws InventoryException {
        ProductFactory sut = new ProductFactory("Deli");

        sut.nextItem("pastrami");
        sut.nextItem("1.0");
        sut.nextItem("1");
        sut.nextItem("2020-04-25");
        sut.nextItem("1.0");
        sut.nextItem("true");

        Product product = sut.build();

        assertTrue(product instanceof PerishableProduct);
        assertTrue(product instanceof VeganisableProduct);
        assertEquals(new VeganisableProduct("Deli", "pastrami", 1.0, 1, "2020-04-25",1.0, true), product);
    }

    @Test
    public void test_Product_Factory_Is_Complete_Product() throws InventoryException {
        ProductFactory sut = new ProductFactory("Cleaning");

        sut.nextItem("ajax");
        boolean isIncompleteOne = sut.isComplete();

        sut.nextItem("1.0");
        boolean isIncompleteTwo = sut.isComplete();

        sut.nextItem("1");

        assertFalse(isIncompleteOne);
        assertFalse(isIncompleteTwo);
        assertTrue(sut.isComplete());

    }

    @Test
    public void test_Product_Factory_Is_Complete_PersishableProduct() throws InventoryException {
        ProductFactory sut = new ProductFactory("Meat");

        sut.nextItem("brisket");
        sut.nextItem("1.0");
        boolean isIncompleteOne = sut.isComplete();

        sut.nextItem("1");
        sut.nextItem("2020-04-25");
        boolean isIncompleteTwo = sut.isComplete();

        sut.nextItem("1.0");

        assertFalse(isIncompleteOne);
        assertFalse(isIncompleteTwo);
        assertTrue(sut.isComplete());

    }

    @Test
    public void test_Product_Factory_Is_Complete_VeganisableProduct() throws InventoryException {
        ProductFactory sut = new ProductFactory("Deli");

        sut.nextItem("pastrami");
        sut.nextItem("1.0");
        boolean isIncompleteOne = sut.isComplete();

        sut.nextItem("1");
        sut.nextItem("2020-04-25");
        sut.nextItem("1.0");
        boolean isIncompleteTwo = sut.isComplete();

        sut.nextItem("true");

        assertFalse(isIncompleteOne);
        assertFalse(isIncompleteTwo);
        assertTrue(sut.isComplete());

    }

    @Test
    public void test_Product_Factory_Next_Item_Invalid_BasePrice() throws InventoryException {
        ProductFactory sut = new ProductFactory("Deli");

        sut.nextItem("pastrami");

        assertFalse(sut.nextItem("$1"));
        assertFalse(sut.nextItem("One dollar"));
    }

    @Test
    public void test_Product_Factory_Next_Item_Invalid_Stock_Quantity() throws InventoryException {
        ProductFactory sut = new ProductFactory("Deli");

        sut.nextItem("pastrami");
        sut.nextItem("1.0");

        assertFalse(sut.nextItem("One"));
        assertFalse(sut.nextItem("1.1"));
    }

    @Test
    public void test_Product_Factory_Next_Item_Invalid_ExpiryDate() throws InventoryException {
        ProductFactory sut = new ProductFactory("Deli");

        sut.nextItem("pastrami");
        sut.nextItem("1.0");
        sut.nextItem("1");

        assertFalse(sut.nextItem("2020-04-31"));
        assertFalse(sut.nextItem("Next Wednesday"));
        assertFalse(sut.nextItem("31/4/2020"));
    }

    @Test
    public void test_Product_Factory_Next_Item_Invalid_Weight() throws InventoryException {
        ProductFactory sut = new ProductFactory("Deli");

        sut.nextItem("pastrami");
        sut.nextItem("1.0");
        sut.nextItem("1");
        sut.nextItem("2020-04-25");

        assertFalse(sut.nextItem("$1"));
        assertFalse(sut.nextItem("One kilo"));

    }

    @Test
    public void test_Product_Factory_Next_Item_Invalid_Veganness() throws InventoryException {
        ProductFactory sut = new ProductFactory("Deli");

        sut.nextItem("pastrami");
        sut.nextItem("1.0");
        sut.nextItem("1");
        sut.nextItem("2020-04-25");
        sut.nextItem("1.0");

        assertFalse(sut.nextItem("yes"));
        assertFalse(sut.nextItem("vegan"));
        assertFalse(sut.nextItem("not vegan"));
    }
}
