package inventoryapp.product;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestProduct {

    @Test
    public void test_Category_Is_Valid() {
        assertTrue(Product.isValidCategory("Cleaning"));
        assertTrue(PerishableProduct.isValidCategory("MEAT"));
        assertTrue(VeganisableProduct.isValidCategory("dElI"));
    }

    @Test
    public void test_Category_Is_Not_Valid() {
        assertFalse(Product.isValidCategory("trousers"));
        assertFalse(PerishableProduct.isValidCategory("cleaning"));
        assertFalse(VeganisableProduct.isValidCategory("Meat"));
    }

    @Test
    public void test_Expiry_Date_Is_Valid() {
        assertTrue(PerishableProduct.isValidDate("2020-02-29"));
        assertTrue(PerishableProduct.isValidDate("2020-01-31"));
        assertTrue(PerishableProduct.isValidDate("2020-12-31"));
    }

    @Test
    public void test_Expiry_Date_Is_Not_Valid() {
        //impossible dates
        assertFalse(PerishableProduct.isValidDate("2019-02-29"));
        assertFalse(PerishableProduct.isValidDate("2020-04-31"));
        assertFalse(PerishableProduct.isValidDate("2021-09-31"));

        //invalid dates
        assertFalse(PerishableProduct.isValidDate("2020-54-15"));
        assertFalse(PerishableProduct.isValidDate("2020-12-81"));
        assertFalse(PerishableProduct.isValidDate("aaaa-bb-cc"));

        //wrong separator
        assertFalse(PerishableProduct.isValidDate("2020/12/05"));
        assertFalse(PerishableProduct.isValidDate("2020.12.05"));
        assertFalse(PerishableProduct.isValidDate("20201205"));
    }

}
