package inventoryapp.exceptions;

public abstract class InventoryException extends Exception {
    public InventoryException() {
    }

    public InventoryException(String message) {
    }

    public InventoryException(String message, Throwable cause) {
    }

    public InventoryException(Throwable cause) {
    }
}
