package inventoryapp.exceptions;

public class InvalidCategoryException extends InventoryException {
    public InvalidCategoryException() {
        super();
    }

    public InvalidCategoryException(String message) {
        super(message);
    }

    public InvalidCategoryException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCategoryException(Throwable cause) {
        super(cause);
    }
}
