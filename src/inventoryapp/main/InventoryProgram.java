package inventoryapp.main;

import inventoryapp.exceptions.InventoryException;
import inventoryapp.product.Product;
import inventoryapp.product.ProductFactory;
import inventoryapp.storedetails.LocalStore;

public class InventoryProgram {
    public static void main(String[] args) {
        InventoryProgram app = new InventoryProgram();
        app.start();
    }

    private void start() {
        LocalStore supermarket = new LocalStore("SuperStore", "22 Top Shop Lane", 20);
        System.out.println("Reviewing " + supermarket.getName() + " (" + supermarket.getLocation() + ") which can store " + supermarket.getMaxInventory() + " total products.");

        boolean running = true;
        while (running) {
            printMainMenu();
            String selection = Keyboard.readInput();
            switch (selection.toLowerCase()) {
                case "1":
                    System.out.println("Current store inventory:");
                    supermarket.printInfo();
                    System.out.println();
                    break;

                case "2":
                    String category = getCategory();
                    System.out.println("Current store inventory for " + category);
                    supermarket.printInfo(category);
                    System.out.println();
                    break;

                case "3":
                    addProducts(supermarket);
                    System.out.println();
                    break;

                case "q":
                    System.out.println("Bye for now");
                    running = false;
                    break;

                default:
                    System.out.println("That was not a valid selection. Please choose again");
                    break;
            }
        }
    }

    private void printMainMenu() {
        System.out.println("Enter 1 to output all the information for all of the products in the inventory");
        System.out.println("Enter 2 to output all the information for all of the products in a selected category");
        System.out.println("Enter 3 to add products to the inventory");
        System.out.println("Enter q to exit program");
    }

    private String getCategory() {
        System.out.println("Select a category:");
        printCategoryMenu();

        String selection = Keyboard.readInput();
        String category = "";
        while (category.equals("")) {
            try {
                category = categoryFromInt(Integer.parseInt(selection));
            } catch (NumberFormatException e) {
                System.out.println("That was not a valid selection. Please enter the number for the category of product you are adding");
                selection = Keyboard.readInput();
            }
        }
        return category;
    }

    private void addProducts(LocalStore supermarket) {
        boolean running = true;
        while (running) {
            String category = getCategory();

            try {
                supermarket.addProduct(buildProduct(category));
            } catch (InventoryException e) {
                System.out.println("Woops, something went wrong. Maybe try again?");    //all input into the buildProduct method is validated so we should never end up here
            }

            System.out.println("Add another new product (Y/N)?");
            String selection = Keyboard.readInput();

            while (true) {
                if (selection.toLowerCase().equals("n")) {
                    running = false;
                    break;
                } else if (selection.toLowerCase().equals("y")) {
                    break;
                } else {
                    System.out.println("That was not a valid selection. Please enter Y or N");
                    selection = Keyboard.readInput();
                }
            }
        }
    }

    private Product buildProduct(String category) throws InventoryException {

        ProductFactory builder = new ProductFactory(category);

        while (!builder.isComplete()) {
            System.out.println(builder.nextItemDescription());

            while (!builder.nextItem(Keyboard.readInput())) {
                System.out.println("That was not a valid selection; please enter again");   //TODO make this more useful
            }
        }

        return builder.build();
    }

    private void printCategoryMenu() {
        System.out.println("1. Meat");
        System.out.println("2. Produce");
        System.out.println("3. Deli");
        System.out.println("4. Bakery");
        System.out.println("5. Cleaning");
    }

    private String categoryFromInt(int input) {
        switch (input) {
            case 1:
                return "Meat";
            case 2:
                return "Produce";
            case 3:
                return "Deli";
            case 4:
                return "Bakery";
            case 5:
                return "Cleaning";
            default:
                throw new NumberFormatException("That's an integer, but not in the range expected");
        }
    }

}
