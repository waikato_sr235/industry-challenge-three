package inventoryapp.product;

public class VeganisableProduct extends PerishableProduct {
    final private static String[] categories = {"deli", "bakery"};
    final protected boolean isVegan;

    public VeganisableProduct(String category, String name, double basePrice, int stockQuantity, String expiryDate, double weight, boolean isVegan) {
        super(category, name, basePrice, stockQuantity, expiryDate, weight);
        this.isVegan = isVegan;
    }

    /**
     * Hides the isValidCategory from its parent class; caution should be used when calling
     * @param category String containing a product category
     * @return true if a valid category for this extension of Product
     */
    public static boolean isValidCategory(String category) {
        for (String categoryName : categories) {
            if (categoryName.equals(category.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public boolean isVegan() {
        return isVegan;
    }

    @Override
    public String toString() {
        String addOn;
        if (isVegan) {
            addOn = "; is vegan";
        }
        else {
            addOn = "; is not vegan";
        }

        return super.toString() + addOn;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) { return false; }


        if (!(obj instanceof VeganisableProduct)) {
            return false;
        }

        VeganisableProduct that = (VeganisableProduct)obj;
        return this.isVegan == that.isVegan;
    }
}
