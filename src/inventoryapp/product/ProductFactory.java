package inventoryapp.product;

import inventoryapp.exceptions.InvalidCategoryException;
import inventoryapp.exceptions.InvalidDateException;
import inventoryapp.exceptions.InventoryException;

/**
 * Static factory methods allow for creation of products; create method is overloaded to allow for all product types
 * Can be instantiated with a category to allow a product to be built detail-by-detail
 * @author Sam Rosenberg
 */
public class ProductFactory {
    final private String category;
    private String name;
    private double basePrice;
    private int stockQuantity;
    private String expiryDate;
    private double weight;
    private boolean isVegan;
    private int currentItem;

    /**
     * Creates a instance of a ProductFactory that allows for detail-by-detail iterative construction
     * @param category the category of product to construct
     * @throws InventoryException if an invalid category is passed in
     */
    public ProductFactory(String category) throws InventoryException {
        if (!Product.isValidCategory(category) && !PerishableProduct.isValidCategory(category) && !VeganisableProduct.isValidCategory(category)) {
            throw new InvalidCategoryException("Invalid category");
        }
        this.category = category;
        this.currentItem = 1;
    }

    /**
     * ProductFactory.build() may return a valid Product object even when this returns false
     * (but not guaranteed so that behaviour should not be relied upon)
     * IsComplete comfirms that a) it is safe to build a product and b) the user has entered a value for each detail
     * (i.e. none of the instance variables are still at their default value)
     * @return true if a user has passed in a value for each product detail
     */
    public boolean isComplete() {
        if (Product.isValidCategory(category)) {
            return currentItem == 4;
        } else if (PerishableProduct.isValidCategory(category)) {
            return currentItem == 6;
        } else {
            return currentItem == 7;
        }
    }

    /**
     * Intended to be used when iteratively adding details
     * Will throw an IllegalArgumentException if all details have been passed in
     * This can be tested by the isComplete() method
     * @return a string description of what the user needs to provide next
     */
    public String nextItemDescription() {
        switch (currentItem) {
            case 1:
                return "Name of product";
            case 2:
                return "Base price of product in dollars";
            case 3:
                return "Quantity in stock";
            case 4:
                return "Expiry date in the format YYYY-MM-DD";
            case 5:
                return "Weight in kg";
            case 6:
                return "Vegan (true/false)";
            default:
                throw new IllegalArgumentException("Invalid currentItem passed to productDetailFromIndex");
        }
    }

    /**
     * Enter the value of the next product detail
     * nextItemDescription() returns a description of the value
     * @param nextItem value to enter
     * @return true if valid input; false means the input has not been added
     */
    public boolean nextItem(String nextItem) {
        switch (currentItem) {
            case 1:
                name = nextItem;
                break;
            case 2:
                try {
                    basePrice = Double.parseDouble(nextItem);
                } catch (NumberFormatException e) {
                    return false;
                }
                break;
            case 3:
                try {
                    stockQuantity = Integer.parseInt(nextItem);
                } catch (NumberFormatException e) {
                    return false;
                }
                break;
            case 4:
                if (!PerishableProduct.isValidDate(nextItem)) { return false; }
                expiryDate = nextItem;
                break;
            case 5:
                try {
                    weight = Double.parseDouble(nextItem);
                } catch (NumberFormatException e) {
                    return false;
                }
                break;
            case 6:
                if (!(nextItem.toLowerCase().equals("true") || nextItem.toLowerCase().equals("false"))) { return false; }
                isVegan = Boolean.parseBoolean(nextItem);
                break;
            default:
                throw new IllegalArgumentException("Invalid index passed to productDetailFromIndex");
        }
        currentItem++;
        return true;
    }

    /**
     * Creates a new Product
     * @return the Product object of the appropriate type for the category given
     * @throws InventoryException if any of values are invalid; this should never be thrown if isComplete() is true
     */
    public Product build() throws InventoryException {
        if (Product.isValidCategory(category)) {
            return ProductFactory.create(category, name, basePrice, stockQuantity);
        } else if (PerishableProduct.isValidCategory(category)) {
            return ProductFactory.create(category, name, basePrice, stockQuantity, expiryDate, weight);
        } else {
            return ProductFactory.create(category, name, basePrice, stockQuantity, expiryDate, weight, isVegan);
        }
    }

    public static Product create(String category, String name, double basePrice, int stockQuantity) throws InventoryException {
        if (!Product.isValidCategory(category)) {
            throw new InvalidCategoryException("Invalid category");
        }
        return new Product(category, name, basePrice, stockQuantity);
    }

    public static Product create(String category, String name, double basePrice, int stockQuantity, String expiryDate, double weight) throws InventoryException {
        if (!PerishableProduct.isValidCategory(category)) {
            throw new InvalidCategoryException("Invalid category");
        }
        if (!PerishableProduct.isValidDate(expiryDate)) {
            throw new InvalidDateException("Invalid date");
        }
        return new PerishableProduct(category, name, basePrice, stockQuantity, expiryDate, weight);
    }

    public static Product create(String category, String name, double basePrice, int stockQuantity, String expiryDate, double weight, boolean isVegan)  throws InventoryException {
        if (!VeganisableProduct.isValidCategory(category)) {
            throw new InvalidCategoryException("Invalid category");
        }
        if (!VeganisableProduct.isValidDate(expiryDate)) {
            throw new InvalidDateException("Invalid date");
        }
        return new VeganisableProduct(category, name, basePrice, stockQuantity, expiryDate, weight, isVegan);
    }
}
