package inventoryapp.product;

public class PerishableProduct extends Product {
    final private static String[] categories = {"produce", "meat"};

    final protected String expiryDate;
    final protected double weight;

    public PerishableProduct(String category, String name, double basePrice, int stockQuantity, String expiryDate, double weight) {
        super(category, name, basePrice, stockQuantity);
        this.expiryDate = expiryDate;
        this.weight = weight;
    }

    /**
     * Validate that a string is a date in the format YYYY-MM-DD; also checks that month and day are valid
     * @param expiryDate a string that is expected to be a valid date
     * @return true if date valid
     */
    public static boolean isValidDate(String expiryDate) {
        boolean isValidFormat = expiryDate.matches("\\d\\d\\d\\d-\\d\\d-\\d\\d");
        if (!isValidFormat) { return false; }

        String[] dateParts = expiryDate.split("-");
        int year = Integer.parseInt(dateParts[0]);
        int month = Integer.parseInt(dateParts[1]);
        int day = Integer.parseInt(dateParts[2]);

        if (month > 12) { return false; }

        if (month == 2) {
            if ((year % 4 == 0) && !((year % 100 == 0) && !(year % 400 == 0))) {    //leap year test; divisible by 4, but not by 100 unless also by 400
                return day <= 29;
            } else {
                return day <= 28;
            }
        } else if ((month % 2 == 0 && month >= 7) || (month % 2 != 0 && month <= 7)) {    //odd numbered months up to july and even numbered after have 31 days
            return day <= 31;
        } else {
            return day <= 30;
        }
    }

    /**
     * Hides the isValidCategory from its parent class; caution should be used when calling
     * @param category String containing a product category
     * @return true if a valid category for this extension of Product
     */
    public static boolean isValidCategory(String category) {
        for (String categoryName : categories) {
            if (categoryName.equals(category.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return super.toString() + "; product weight: " + weight + "kg; expiry: " + expiryDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }

        if (!(obj instanceof PerishableProduct)) {
            return false;
        }

        PerishableProduct that = (PerishableProduct)obj;
        return this.expiryDate.equals(that.expiryDate) &&
                Math.abs(this.weight - that.weight) < 1e-15;
    }
}
