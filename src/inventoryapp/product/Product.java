package inventoryapp.product;

public class Product {
    final private static String[] categories = {"cleaning"};

    final protected String category;
    final protected String name;
    final protected double basePrice;
    protected int stockQuantity;

    public Product(String category, String name, double basePrice, int stockQuantity) {
        this.name = name;
        this.category = category;
        this.basePrice = basePrice;
        this.stockQuantity = stockQuantity;
    }

    /**
     * Child classes should hide this with an adjusted version appropriate for that specific extension
     * @param category String containing a product category
     * @return true if a valid category for this extension of Product
     */
    public static boolean isValidCategory(String category) {
        for (String categoryName : categories) {
            if (categoryName.equals(category.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    @Override
    public String toString() {
        return name + " (" + category + "); price: $" + basePrice + "; in stock: " + stockQuantity;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Product)) {
            return false;
        }

        Product that = (Product)obj;
        return this.category.equals(that.category) &&
                this.name.equals(that.name) &&
                Math.abs(this.basePrice - that.basePrice) < 1e-15 &&
                this.stockQuantity == that.stockQuantity;

    }
}
