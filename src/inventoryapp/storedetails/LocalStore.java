package inventoryapp.storedetails;

import inventoryapp.product.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * A class that represents the current state of a given supermarket
 * @author Sam Rosenberg
 */
public class LocalStore {
    private String name;
    final private String location;
    private int maxInventory;
    final private List<Product> inventory;

    public LocalStore(String name, String location, int maxInventory) {
        this.name = name;
        this.location = location;
        this.maxInventory = maxInventory;
        this.inventory = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public int getMaxInventory() {
        return maxInventory;
    }

    public void setMaxInventory(int maxInventory) {
        this.maxInventory = maxInventory;
    }

    /**
     * Adds a product to the store inventory
     * @param product product to add; ProductFactory exists
     * @return false if inventory is full
     */
    public boolean addProduct(Product product) {
        if (inventory.size() >= maxInventory) {
            return false;
        }

        inventory.add(product);
        return true;
    }

    /**
     * Prints to console the details for each item in inventory
     */
    public void printInfo() {
        for (Product product : inventory) {
            System.out.println(product.toString());
        }
    }

    /**
     * Prints to console the details for each item of a selected category in inventory
     * @param category selection to print
     */
    public void printInfo(String category) {
        for (Product product : inventory) {
            if (product.getCategory().equals(category)) {
                System.out.println(product.toString());
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof LocalStore)) {
            return false;
        }

        LocalStore that = (LocalStore) obj;
        return this.name.equals(that.name) &&
                this.location.equals(that.location) &&
                this.inventory.equals(that.inventory) &&
                this.maxInventory == that.maxInventory;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append("Store: ");
        result.append(name);

        result.append(" At: ");
        result.append(location);

        result.append(" Can store: ");
        result.append(maxInventory);
        result.append(" products.\n");

        result.append("Current stock:\n");
        for (Product product : inventory) {
            result.append(product.toString());
        }
        return result.toString();


    }
}
